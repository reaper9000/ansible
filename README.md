
```
python -m venv .ve
source .ve/bin/activate
pip install -r requirements.txt
export AWS_ACCESS_KEY_ID=<YOUR_AWS_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
./ec2.py
```